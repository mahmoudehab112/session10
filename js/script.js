'use strict'
// Hello World!
console.log('Hello World!');
let parameterVariable = 'Welcome to 10 days of javascript'
console.log(parameterVariable);
//Data types
let firstIntegar = '4';
let firstDecimal = '4.0';
let firstString = 'HackerRank';
let secondIntegar = '12';
let secondDecimal = '4.32';
let secondString = ' is the best place to learn and practice coding!';

console.log(parseInt(firstIntegar) + parseInt(secondIntegar))
console.log(parseFloat(firstDecimal)+parseFloat(secondDecimal))
console.log(firstString + secondString)
//Arithmatic Operators
let length = 3 ;
let Width  = 4.5;
const area = length * Width ;
const perimeter = 2 * (length + Width)

console.log(area)
console.log(perimeter)
//Functions
function factorial (n)
{ if (n === 0)  { 
   return 1; 
 } 
 return n * factorial(n-1);
}
 console.log(factorial(4))
//Let and Const
const PI = Math.PI ;
let r = 2.6 ;
const areaCircle = PI * r * r ;
const perimeterCircle = 2 * PI * r ;

console.log(areaCircle)
console.log(perimeterCircle)

//Conditional Statements : if and Else
function getGrade(score) {
  if (score > 25 && score <= 30) {
      return 'A';
   } else if (score > 20 && score <= 25) {
      return 'B';
   } else if (score >15 && score <=20) {
      return 'C';
   } else if (score >10 && score <=15) {
      return 'D';
   } else if (score >5 && score <=10)
      return 'E';
      else  if  (score >=0 && score <=5)
      return 'F';
}
console.log(getGrade(11));








